#!/usr/bin/bash
set -x
docker volume create openwrt-mirror

docker run -d --name openwrt-mirror-updater -v openwrt-mirror:/data/rw \
    --restart=always \
    registry.gitlab.com/jkushmaul/docker-openwrt-mirror0r:latest \
        -d \
        -s 60 \
        -c "releases" \
        -t "mvebu/cortexa9" -t "apm821xx/nand" \
        -a "arm_cortex-a9_vfpv3-d16" -a "powerpc_464fp" \
        -v "19.07.7" \
        -l "/data/rw/" \
        -r "rsync://downloads.openwrt.org/downloads" \

