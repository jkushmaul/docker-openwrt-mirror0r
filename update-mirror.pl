#!/usr/bin/perl -w
use strict;
use Getopt::Long;
use Data::Dumper;
use File::Spec;

$Data::Dumper::Indent = 1;
$Data::Dumper::Sortkeys = 1;

# disable output buffering
binmode( STDOUT, ":unix" );

sub print_help() {
    print "Usage: update_mirror.sh [OPTIONS]
    -r     rsync:// url source (Required)
    -l     local path destination (Required)
    -b     bandwidth in bytes (Default: 1000)
    -s     loop with sleep time in seconds (Default: 86400)
    -f     disable safety checks (Default: false)
    -c     PATTERN filter channel (Default: *)
    -v     PATTERN filter version (Default: *)
    -t     PATTERN filter image target (Default: *)
    -a     PATTERN filter package arch (Default: *)
    -x     print rsync command and exit
    -d     --delete
    -q     quiet
    -h     show usage
";
}

if (!$ARGV[1]) {
   print_help();
   exit 1;
}

my @F_CHANNEL_ARR=();
my @F_VERSION_ARR=();
my @F_IMAGE_TARGET_ARR=();
my @F_PACKAGE_ARCH_ARR=();
my %OPTS = (
    "HELP" => 0,
    "DELETE" => 0,
    "QUIET" => 0,
    "EXIT" => 0,
    "FORCE" => 0,
    "RSYNC" => "",
    "LOCAL" => "",
    "BANDWIDTH" => 1024,
    "SLEEP" => 0,
    "F_CHANNEL" => \@F_CHANNEL_ARR,
    "F_VERSION" => \@F_VERSION_ARR,
    "F_IMAGE_TARGET" => \@F_IMAGE_TARGET_ARR,
    "F_PACKAGE_ARCH" => \@F_PACKAGE_ARCH_ARR 
);

Getopt::Long::Configure ("bundling");
GetOptions(
			"help|h" => \$OPTS{"HELP"},
            "delete|d" => \$OPTS{"DELETE"},
            "quiet|q" => \$OPTS{"QUIET"},
            "exit|x" => \$OPTS{"EXIT"},
			"force|f" => \$OPTS{"FORCE"},
            "rsync|r=s"=> \$OPTS{"RSYNC"},
            "local|l=s"=> \$OPTS{"LOCAL"},
			"bandwidth|b=i" => \$OPTS{"BANDWIDTH"},
			"sleep|s=i" => \$OPTS{"SLEEP"},
		    "channel|c=s" => \@F_CHANNEL_ARR,
			"version|v=s" => \@F_VERSION_ARR,
			"target|t=s"	=> \@F_IMAGE_TARGET_ARR,
            "arch|a=s" => \@F_PACKAGE_ARCH_ARR
          ) or die(print_help());

if ($ARGV[0]) {
    print "Extra parametersby Getopt::Long\n";
    foreach (@ARGV) {
        print "$_\n";
    }
    print_help();
    exit 1;
}

print "HELP: $OPTS{HELP}\n";
if ($OPTS{HELP} > 0) {
  print_help();
}

if ($OPTS{"RSYNC"} eq "") {
    die("--rsync is required");
}
if ($OPTS{"LOCAL"} eq "") {
    die("--local is required");
}

### Build the list of filtered dirs for rsync
sub build_filters {
   my $full_path = $_[0];

   my @filters = ();
   my @dirs = File::Spec->splitdir($full_path);
   my $current_path = "";
   shift @dirs;
   foreach my $d (@dirs)
   {
        $current_path="$current_path/$d";
        push @filters, "--filter=+ $current_path";
   }
   push @filters, "--filter=+ $current_path/***";
   return @filters;
}

push @F_CHANNEL_ARR, "*" if scalar @F_CHANNEL_ARR == 0;
push @F_VERSION_ARR, "*" if scalar @F_VERSION_ARR == 0;
push @F_IMAGE_TARGET_ARR, "*" if scalar @F_IMAGE_TARGET_ARR == 0;
push @F_PACKAGE_ARCH_ARR, "*" if scalar @F_PACKAGE_ARCH_ARR == 0;

my @FILTERS=();
foreach my $c (@F_CHANNEL_ARR) {
  my $c_path = "/$c";
  foreach my $v (@F_VERSION_ARR) {
    my $v_path = "$c_path/$v";
    foreach my $t (@F_IMAGE_TARGET_ARR) {
        my $t_path = "$v_path/targets/$t";
        push @FILTERS, build_filters($t_path);
    }
    #if 19.07.7, then only 19.07
    #if 19*, *, 19.07* then verbatim
    my ($vsub) = $v =~ m/([\d]{2}\.[\d]{2}|.*)/;
    foreach my $a (@F_PACKAGE_ARCH_ARR) {
        my $a_path = "$c_path/packages-$vsub/$a";
        push @FILTERS, build_filters($a_path);
    }
  }
}

#save exclusion for last
push @FILTERS, "--filter=- *";

my @cmd=("rsync", "-aSHP");
if ($OPTS{QUIET} == 0) {
    push @cmd, "-v"
}
if ($OPTS{DELETE} > 0) {
    push @cmd, "--delete";
}
if ($OPTS{BANDWIDTH} > 0) {
    push @cmd, "--bwlimit=$OPTS{BANDWIDTH}"
}
push @cmd, @FILTERS;
push @cmd, $OPTS{RSYNC};
push @cmd, $OPTS{LOCAL};

my @shellcmd=();
foreach my $v (@cmd)
{
  push @shellcmd, "\"$v\"";
}


$SIG{INT} = sub { die "Caught sigint $!, exiting" };
$SIG{TERM} = sub { die "Caught sigterm $!, exiting" };

my $RUN=1;
while($RUN > 0) {
  print "Executing rsync: @shellcmd\n";
  if ($OPTS{EXIT} > 0) {
    last;
  }
  system(@cmd);
  print "rsync complete\n";
  if ($OPTS{SLEEP} > 0) {
     print "Sleeping for $OPTS{SLEEP} seconds\n";
     sleep $OPTS{SLEEP};
  } else {
     last;
  }
}

print "Finished\n";
