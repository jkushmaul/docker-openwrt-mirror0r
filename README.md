# OpenWRT Downloads Mirror Docker

This allows one to setup a mirror for either a full-blown mirror, or
in my case, a smaller, more focused, local mirror of specific versions, targets and archs.


## Usage

### Nginx container

Just your basic nginx.  This creates the mirror volume starting nginx to serve 
that path read only on port 80.

```
docker volume create openwrt-mirror
docker run -d \
    -v openwrt-mirror:/usr/share/nginx/html:ro \
    -p 80:80 \
    --name=openwrt-mirror-nginx \
    nginx
```

### mirror0r container

This would launch the mirror0r container with the openwrt-mirror volume in rw.
It will only pull `releases` channel, `mvebu/cortexa9` and `apm821xx/nand` targets 
and only `arm_cortex-a9-vfpv3-d16` and `powerpc_464fp` arch packages for version `19.07.7`

```
 docker volume create openwrt-mirror
 docker run -d --name openwrt-mirror-updater -v openwrt-mirror:/data/rw \
      registry.gitlab.com/jkushmaul/docker-openwrt-mirror0r \
          -d \
          -s 60 \
          -c "releases" \
          -t "mvebu/cortexa9" -t "apm821xx/nand" \
          -a "arm_cortex-a9_vfpv3-d16" -a "powerpc_464fp" \
          -v "19.07.7" \
          -l "/data/rw/" \
          -r "rsync://downloads.openwrt.org/downloads" \
```

### mirror0r options

This is just a perl script to make managing rsync easier.

```
./update-mirror.pl --help
Usage: update_mirror.sh [OPTIONS]
    -r     rsync:// url source (Required)
    -l     local path destination (Required)
    -b     bandwidth in bytes (Default: 1000)
    -s     loop with sleep time in seconds (Default: 86400)
    -f     disable safety checks (Default: false)
    -c     PATTERN filter channel (Default: *)
    -v     PATTERN filter version (Default: *)
    -t     PATTERN filter image target (Default: *)
    -a     PATTERN filter package arch (Default: *)
    -x     print rsync command and exit
    -d     --delete
    -q     quiet
    -h     show usage
```

## Future

I intend to implement some smarter version semantics, such as `19.07.7+` to indicate
anything higher.  rsync does not make this easy to do as there is no hook.  So the versions
will need to be fetched independantly of the rsync process and filter rules written before each rsync
command is run.

