docker stop openwrt-mirror-nginx;
docker rm openwrt-mirror-nginx;

docker volume create openwrt-mirror
docker volume create openwrt-mirror-nginx-etc


docker run --rm -t -v $PWD/nginx.conf:/nginx.conf -v openwrt-mirror-nginx-etc:/etc/nginx nginx /bin/bash  -c "cp /nginx.conf /etc/nginx/nginx.conf"
docker run -d \
    -v openwrt-mirror:/usr/share/nginx/html:ro \
    -v openwrt-mirror-nginx-etc:/etc/nginx \
    --restart=always \
    -p 80:80 \
    --name=openwrt-mirror-nginx \
    nginx
