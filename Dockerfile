FROM alpine:latest

RUN apk --no-cache add rsync bash perl

ADD ./update-mirror.pl /usr/local/bin/update-mirror.pl

RUN chmod +x /usr/local/bin/*

ENTRYPOINT ["/usr/local/bin/update-mirror.pl"]
